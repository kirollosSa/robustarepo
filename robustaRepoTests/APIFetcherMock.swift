//
//  APIFetcherMock.swift
//  robustaRepoTests
//
//  Created by Kirollos Maged on 12/20/20.
//

import Foundation
@testable import robustaRepo

class APIFetcherMock: Fetcher {
    func fetch<ResponseType>(requestUrl: String, mappingInResponse response: ResponseType.Type, onSuccess: @escaping (ResponseType) -> Void, onFailure: @escaping (Error) -> Void) where ResponseType : Decodable, ResponseType : Encodable {
        let bundle = Bundle(for: APIFetcherMock.self)
        if let path = bundle.path(forResource: requestUrl, ofType: "json") {
            if let data = try? NSData(contentsOfFile: path, options: NSData.ReadingOptions.mappedIfSafe) {
                let jsonDecoder = JSONDecoder()
                if let obj = try? jsonDecoder.decode(response, from: data as Data) {
                    onSuccess(obj)
                }
            }
        }
        print("any thing")
    }
    
}
