//
//  ReposListServiceCallTests.swift
//  robustaRepoTests
//
//  Created by Kirollos Maged on 12/20/20.
//

import XCTest
@testable import robustaRepo

class ReposListServiceCallTests: XCTestCase {
    
    var aPIFetcher: APIFetcher!

    override func setUpWithError() throws {
        aPIFetcher = APIFetcher()
    }

    override func tearDownWithError() throws {
        aPIFetcher = nil
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testReposList() {
        let expect = expectation(description: "API Replied")
        let url = APIs.githubRepos.url()
            APIFetcher().fetch(requestUrl: url, mappingInResponse: [Repo].self) { response in
                expect.fulfill()
                XCTAssert(response.count > 0, "API responds with data")
            } onFailure: { error in
                
            }
        wait(for: [expect], timeout: 10)
    }
    
    func testReposListNotNill() {
        let expect = expectation(description: "API Replied")
        let url = APIs.githubRepos.url()
            APIFetcher().fetch(requestUrl: url, mappingInResponse: [Repo].self) { response in
                expect.fulfill()
                XCTAssertNotNil(response, "")
            } onFailure: { error in
                
            }
        wait(for: [expect], timeout: 10)
    }

}
