//
//  ReposListViewModelTests.swift
//  robustaRepoTests
//
//  Created by Kirollos Maged on 12/20/20.
//

import XCTest
@testable import robustaRepo

class ReposListViewModelTests: XCTestCase {

    var reposListViewModel: ReposListViewModel!
    var disposableBag: DisposableBag!
    
    override func setUpWithError() throws {
        reposListViewModel = ReposListViewModel(aPIFetcher: APIFetcherMock())
        disposableBag = DisposableBag()
    }

    override func tearDownWithError() throws {
        reposListViewModel = nil
        disposableBag = nil
    }

    func testReposListSuccess() {
        disposableBag.add(reposListViewModel.reposList.bind({ responds in
            XCTAssertNotNil(responds)
        }))
        
        reposListViewModel.url = "responseSuccess"
        reposListViewModel.getReposList()
    }
    
    func testSearchFunction() {
        let expect = expectation(description: "search wait")
        disposableBag.add(reposListViewModel.searchReposList.bind({ responds in
            expect.fulfill()
            // we know that we have repo with name "grit", so we will check if search working fine or no
            let searchSucceded = responds.contains { repo -> Bool in
                return repo.name == "grit"
            }
            XCTAssertTrue(searchSucceded)
        }))
        
        reposListViewModel.url = "responseSuccess"
        reposListViewModel.getReposList()
        reposListViewModel.searchFor(query: "grit")
        wait(for: [expect], timeout: 2)
    }
    
    func testNoDataSearchFunction() {
        let expect = expectation(description: "search wait")
        disposableBag.add(reposListViewModel.searchReposList.bind({ responds in
            expect.fulfill()
            // we know that we have repo with name "grit", so we will check if search working fine or no
            let searchSucceded = responds.contains { repo -> Bool in
                return repo.name == "XYZ"
            }
            XCTAssertFalse(searchSucceded)
        }))
        
        reposListViewModel.url = "responseSuccess"
        reposListViewModel.getReposList()
        reposListViewModel.searchFor(query: "XYZ")
        wait(for: [expect], timeout: 2)
    }

}
