//
//  UICollectionView+extension.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/18/20.
//

import UIKit

let kCollectionViewFooterSize = CGSize(width: 60.0, height: 40.0)

extension UICollectionView {
    func registerCell<T: BaseCollectionViewCell>(withCellType: T.Type) {
        self.register(UINib(nibName: T.nibName, bundle: nil), forCellWithReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableCell<Base: BaseCollectionViewCell>(forIndexPath indexPath: IndexPath) -> Base {
        if let cell = self.dequeueReusableCell(withReuseIdentifier: Base.reuseIdentifier, for: indexPath) as? Base {
            cell.setupView()
            return cell
        } else {
            fatalError("CollectionView can not dequeue cell with reusableIdentifier '\(Base.reuseIdentifier)' at indexPath '\(indexPath)'")
        }
    }

    func registerFooterView<T: BaseCollectionReusableView>(withViewType: T.Type)  {
        self.register(UINib(nibName: T.nibName, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableFooterView<T: BaseCollectionReusableView> (forIndexPath indexPath: IndexPath) -> T {
        if let footer = self.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T {
            footer.setupView()
            return footer
        } else {
            fatalError("CollectionView can not dequeue footer view with reusableIdentifier '\(T.reuseIdentifier)' at indexPath '\(indexPath)'")
        }
    }

}

