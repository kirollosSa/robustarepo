//
//  String+extension.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/17/20.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.dropFirst()
    }
}
