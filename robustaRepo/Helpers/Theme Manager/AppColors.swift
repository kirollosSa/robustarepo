//
//  AppColors.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/16/20.
//

import UIKit

struct AppColors {
    static let clear = UIColor.clear
    static let blue = UIColor.blue
    static let blackWithOpacity50 = UIColor.black.withAlphaComponent(0.5)
    static let black = UIColor.black
    static let white = UIColor.white
    static let lightGray = UIColor(hexString: "#F7EAE8")
}
