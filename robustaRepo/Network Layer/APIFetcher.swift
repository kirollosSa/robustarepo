//
//  APIFetcher.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/16/20.
//

import Foundation

struct APIFetcher: Fetcher {
    
    func fetch<ResponseType>(requestUrl: String, mappingInResponse response: ResponseType.Type, onSuccess: @escaping (ResponseType) -> Void, onFailure: @escaping (Error) -> Void) where ResponseType : Decodable, ResponseType : Encodable {
        guard let url = URL(string: requestUrl) else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) in
            if let _error = error {
                onFailure(_error)
            }
            
            if let data = data,
               let respondsData = try? JSONDecoder().decode(ResponseType.self, from: data) {
                onSuccess(respondsData)
            }
        })
        task.resume()
    }
}


