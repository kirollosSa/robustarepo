//
//  AppInviroment.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/16/20.
//

import Foundation

enum AppEnvironment {
    case dev
    case testing
    case live
    case staging
}

/// kAppEnvironment:- we are switching between environments by changing kAppEnvironment value
let kAppEnvironment: AppEnvironment = .staging

var kBaseURLComponents: URLComponents {
    var urlComponents = URLComponents()
    urlComponents.scheme = kScheme
    urlComponents.host = kHost
    if kPort != nil {
        urlComponents.port = kPort
    }
    return urlComponents
}

var kBaseURL: String {
    return kBaseURLComponents.url?.absoluteString ?? ""
}

/// change scheme based on our servers scheme setup
var kScheme: String {
    switch kAppEnvironment {
    case .dev:          return "http"
    case .staging:      return "https"
    case .testing:      return "http"
    case .live:         return "https"
    }
}

var kHost: String {
    switch kAppEnvironment {
    case .dev:          return ""
    case .staging:      return "api.github.com"
    case .testing:      return ""
    case .live:         return "api.github.com"
    }
}

var kPort: Int? {
    switch kAppEnvironment {
    case .dev:          return nil
    case .staging:      return nil
    case .testing:      return nil
    case .live:         return nil
    }
}
