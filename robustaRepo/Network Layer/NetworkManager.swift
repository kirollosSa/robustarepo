//
//  NetworkManager.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/16/20.
//

import Foundation

protocol Netwokable {
    func callService(from url: URL,completionHandler: @escaping ((Data, Codable, Error) -> Void))
}

