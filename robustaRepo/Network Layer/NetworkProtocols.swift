//
//  NetworkProtocols.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/16/20.
//

import Foundation

protocol Fetcher {
    func fetch<ResponseType: Codable> (requestUrl: String, mappingInResponse response: ResponseType.Type, onSuccess: @escaping (ResponseType) -> Void, onFailure: @escaping (Error) -> Void )
}
