//
//  APIs.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/16/20.
//

import Foundation

/// add all endpoint here
enum APIs {
    case githubRepos
    
    func url() -> String {
        switch self {
        case .githubRepos:
            return kBaseURL + "/repositories"
        }
    }
}
