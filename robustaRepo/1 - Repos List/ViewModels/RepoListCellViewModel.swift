//
//  RepoListCellViewModel.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/18/20.
//

import Foundation

class RepoListCellViewModel: BaseViewModel {
    var loaderState = Bindable<LoadingIndicatorState>(.hidden)
    var repoName: String
    var repoOwnerName: String
    var repoOwnerImage: String
    var creationDate: String
    
    init(repoName: String,repoOwnerName: String,repoOwnerImage: String,creationDate: String) {
        self.repoName = repoName
        self.repoOwnerName = repoOwnerName
        self.repoOwnerImage = repoOwnerImage
        self.creationDate = creationDate
    }
}
