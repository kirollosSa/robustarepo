//
//  ReposListViewModel.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/17/20.
//

import Foundation

class ReposListViewModel: BaseViewModel, ListingSearch {
    
    var query: String = ""
    var loaderState = Bindable<LoadingIndicatorState>(.hidden)
    var allResponseList = [Repo]()
    var reposList = Bindable<[Repo]>([])
    var searchReposList = Bindable<[Repo]>([])
    var errorMessage = Bindable<String?>(nil)
    var shouldPaginate = false
    var pageNumber = 1
    var maximumPageRepos = 10
    private var searchTask: DispatchWorkItem?
    private var minimumQueryLength: Int = 2
    private var isDataFromSearch: Bool = false
    var url: String = APIs.githubRepos.url()
    var aPIFetcher: Fetcher!
    
    var listCount : Int {
        if isDataFromSearch {
            return searchReposList.value.count
        } else {
            return reposList.value.count
        }
    }
    
    enum PageState {
        case first
        case next
    }
    
    init(aPIFetcher: Fetcher = APIFetcher()) {
        self.aPIFetcher = aPIFetcher
    }
    
    func getReposList(atPage: PageState = .first,shouldShowLoader: Bool = false) {
        if atPage == .first {
            loaderState.value = .shown
            aPIFetcher.fetch(requestUrl: url, mappingInResponse: [Repo].self) { [weak self] response in
                self?.loaderState.value = .hidden
                self?.allResponseList = response
                // first10 to handle Pagination
                let first10 = response.prefix(self!.maximumPageRepos)
                self?.reposList.value = Array(first10)
                self?.shouldPaginate = true
            } onFailure: { [weak self] error in
                self?.loaderState.value = .hidden
                self?.errorMessage.value = error.localizedDescription
            }
        }
        else {
            // waiting for 2 seconds to simulate service call
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
                self?.pageNumber += 1
                if self!.reposList.value.count < self!.allResponseList.count {
                    self!.reposList.value = Array(self!.allResponseList.prefix(self!.pageNumber * 10))
                } else {
                    self?.shouldPaginate = false
                }
            }
        }
    }
    
    func getCellViewModel(for index: Int) -> BaseViewModel {
        if isDataFromSearch {
            let cellViewModel = RepoListCellViewModel(repoName: searchReposList.value[index].name ?? "_", repoOwnerName: searchReposList.value[index].owner?.login ?? "_", repoOwnerImage: searchReposList.value[index].owner?.avatar_url ?? "", creationDate: "")
            return cellViewModel
        } else {
            let cellViewModel = RepoListCellViewModel(repoName: reposList.value[index].name ?? "_", repoOwnerName: reposList.value[index].owner?.login ?? "_", repoOwnerImage: reposList.value[index].owner?.avatar_url ?? "", creationDate: "")
            return cellViewModel
        }
    }
    
    func getRepoModel(for index: Int) -> Repo {
        if isDataFromSearch {
            return searchReposList.value[index]
        } else {
            return reposList.value[index]
        }
    }
    
    func getNextPageIfNeeded(_ index: Int) {
        guard shouldPaginate else { return }
        if self.reposList.value[index].id == self.reposList.value.last?.id {
            getReposList(atPage: .next)
        }
    }
    
    func searchFor(query: String) {
        didLookupFor(query: query)
    }
    
    func didLookupFor(query: String) {
        // throttling search calls temporarily
        searchTask?.cancel()
        guard query != self.query else {
            return
        }
        self.query = query
        searchTask = DispatchWorkItem {
            [weak self] in
            self?.searchLogicFor()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: searchTask!)
    }
    
    ///offline search logic based on all response list
    func searchLogicFor() {
        if query.count >= minimumQueryLength {
            isDataFromSearch = true
            shouldPaginate = false
            let matchingNames = allResponseList.filter({
                $0.name?.range(of: query, options: .caseInsensitive) != nil
            })
            searchReposList.value = matchingNames
        } else {
            isDataFromSearch = false
            shouldPaginate = true
            searchReposList.value = reposList.value
        }
    }
}
