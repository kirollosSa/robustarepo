//
//  Repo.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/16/20.
//

import Foundation

struct Repo : Codable {
    let id : Int?
    let node_id : String?
    let name : String?
    let full_name : String?
    let private_ : Bool?
    let owner : Owner?
    let html_url : String?
    let description : String?
    let fork : Bool?
    let url : String?
    let forks_url : String?
    let keys_url : String?
    let collaborators_url : String?
    let teams_url : String?
    let hooks_url : String?
    let issue_events_url : String?
    let events_url : String?
    let assignees_url : String?
    let branches_url : String?
    let tags_url : String?
    let blobs_url : String?
    let git_tags_url : String?
    let git_refs_url : String?
    let trees_url : String?
    let statuses_url : String?
    let languages_url : String?
    let stargazers_url : String?
    let contributors_url : String?
    let subscribers_url : String?
    let subscription_url : String?
    let commits_url : String?
    let git_commits_url : String?
    let comments_url : String?
    let issue_comment_url : String?
    let contents_url : String?
    let compare_url : String?
    let merges_url : String?
    let archive_url : String?
    let downloads_url : String?
    let issues_url : String?
    let pulls_url : String?
    let milestones_url : String?
    let notifications_url : String?
    let labels_url : String?
    let releases_url : String?
    let deployments_url : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case node_id = "node_id"
        case name = "name"
        case full_name = "full_name"
        case private_ = "private"
        case owner = "owner"
        case html_url = "html_url"
        case description = "description"
        case fork = "fork"
        case url = "url"
        case forks_url = "forks_url"
        case keys_url = "keys_url"
        case collaborators_url = "collaborators_url"
        case teams_url = "teams_url"
        case hooks_url = "hooks_url"
        case issue_events_url = "issue_events_url"
        case events_url = "events_url"
        case assignees_url = "assignees_url"
        case branches_url = "branches_url"
        case tags_url = "tags_url"
        case blobs_url = "blobs_url"
        case git_tags_url = "git_tags_url"
        case git_refs_url = "git_refs_url"
        case trees_url = "trees_url"
        case statuses_url = "statuses_url"
        case languages_url = "languages_url"
        case stargazers_url = "stargazers_url"
        case contributors_url = "contributors_url"
        case subscribers_url = "subscribers_url"
        case subscription_url = "subscription_url"
        case commits_url = "commits_url"
        case git_commits_url = "git_commits_url"
        case comments_url = "comments_url"
        case issue_comment_url = "issue_comment_url"
        case contents_url = "contents_url"
        case compare_url = "compare_url"
        case merges_url = "merges_url"
        case archive_url = "archive_url"
        case downloads_url = "downloads_url"
        case issues_url = "issues_url"
        case pulls_url = "pulls_url"
        case milestones_url = "milestones_url"
        case notifications_url = "notifications_url"
        case labels_url = "labels_url"
        case releases_url = "releases_url"
        case deployments_url = "deployments_url"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        node_id = try values.decodeIfPresent(String.self, forKey: .node_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        full_name = try values.decodeIfPresent(String.self, forKey: .full_name)
        private_ = try values.decodeIfPresent(Bool.self, forKey: .private_)
        owner = try values.decodeIfPresent(Owner.self, forKey: .owner)
        html_url = try values.decodeIfPresent(String.self, forKey: .html_url)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        fork = try values.decodeIfPresent(Bool.self, forKey: .fork)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        forks_url = try values.decodeIfPresent(String.self, forKey: .forks_url)
        keys_url = try values.decodeIfPresent(String.self, forKey: .keys_url)
        collaborators_url = try values.decodeIfPresent(String.self, forKey: .collaborators_url)
        teams_url = try values.decodeIfPresent(String.self, forKey: .teams_url)
        hooks_url = try values.decodeIfPresent(String.self, forKey: .hooks_url)
        issue_events_url = try values.decodeIfPresent(String.self, forKey: .issue_events_url)
        events_url = try values.decodeIfPresent(String.self, forKey: .events_url)
        assignees_url = try values.decodeIfPresent(String.self, forKey: .assignees_url)
        branches_url = try values.decodeIfPresent(String.self, forKey: .branches_url)
        tags_url = try values.decodeIfPresent(String.self, forKey: .tags_url)
        blobs_url = try values.decodeIfPresent(String.self, forKey: .blobs_url)
        git_tags_url = try values.decodeIfPresent(String.self, forKey: .git_tags_url)
        git_refs_url = try values.decodeIfPresent(String.self, forKey: .git_refs_url)
        trees_url = try values.decodeIfPresent(String.self, forKey: .trees_url)
        statuses_url = try values.decodeIfPresent(String.self, forKey: .statuses_url)
        languages_url = try values.decodeIfPresent(String.self, forKey: .languages_url)
        stargazers_url = try values.decodeIfPresent(String.self, forKey: .stargazers_url)
        contributors_url = try values.decodeIfPresent(String.self, forKey: .contributors_url)
        subscribers_url = try values.decodeIfPresent(String.self, forKey: .subscribers_url)
        subscription_url = try values.decodeIfPresent(String.self, forKey: .subscription_url)
        commits_url = try values.decodeIfPresent(String.self, forKey: .commits_url)
        git_commits_url = try values.decodeIfPresent(String.self, forKey: .git_commits_url)
        comments_url = try values.decodeIfPresent(String.self, forKey: .comments_url)
        issue_comment_url = try values.decodeIfPresent(String.self, forKey: .issue_comment_url)
        contents_url = try values.decodeIfPresent(String.self, forKey: .contents_url)
        compare_url = try values.decodeIfPresent(String.self, forKey: .compare_url)
        merges_url = try values.decodeIfPresent(String.self, forKey: .merges_url)
        archive_url = try values.decodeIfPresent(String.self, forKey: .archive_url)
        downloads_url = try values.decodeIfPresent(String.self, forKey: .downloads_url)
        issues_url = try values.decodeIfPresent(String.self, forKey: .issues_url)
        pulls_url = try values.decodeIfPresent(String.self, forKey: .pulls_url)
        milestones_url = try values.decodeIfPresent(String.self, forKey: .milestones_url)
        notifications_url = try values.decodeIfPresent(String.self, forKey: .notifications_url)
        labels_url = try values.decodeIfPresent(String.self, forKey: .labels_url)
        releases_url = try values.decodeIfPresent(String.self, forKey: .releases_url)
        deployments_url = try values.decodeIfPresent(String.self, forKey: .deployments_url)
    }

}
