//
//  RepoListCollectionViewCell.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/18/20.
//

import UIKit

class RepoListCollectionViewCell: BaseCollectionViewCell {
    
    var viewModel: RepoListCellViewModel!
    
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cellWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var ownerImage: UIImageView!
    @IBOutlet weak var repoName: UILabel!
    @IBOutlet weak var ownerName: UILabel!
    @IBOutlet weak var creationDate: UILabel!
    var cellWidth : CGFloat!
    var onReuse: () -> Void = {}
    
    override func prepareForReuse() {
        super.prepareForReuse()
        ownerImage.image = nil
        ownerImage.cancelImageLoad()
        repoName.text = ""
        ownerName.text = ""
        creationDate.text = ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupView() {
        self.mainView.setCornerRadius()
        self.mainView.backgroundColor = AppColors.lightGray
    }
    
    func configure(with viewModel: RepoListCellViewModel) {
        repoName.text = viewModel.repoName
        ownerName.text = viewModel.repoOwnerName
        creationDate.text = viewModel.creationDate
        guard let url = URL(string: viewModel.repoOwnerImage) else { return }
        ownerImage.loadImage(from: url)
    }
}
