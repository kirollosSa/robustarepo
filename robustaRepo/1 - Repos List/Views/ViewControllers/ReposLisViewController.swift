//
//  ReposLisViewController.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/17/20.
//

import UIKit

class ReposLisViewController: BaseViewController, HasViewModel {
    
    @IBOutlet weak var noDataFoundlbl: UILabel!
    @IBOutlet weak var searchViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchView: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel: ReposListViewModel! = ReposListViewModel()
    
    //MARK:- VC life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        showNavigationBar()
    }

    override func setupView() {
        super.setupView()
        setupSearchView()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.keyboardDismissMode = .onDrag
    }
    
    override func bindData() {
        super.bindData()
        viewModel.getReposList()
    }
    
    override func registerNib() {
        collectionView.registerCell(withCellType: RepoListCollectionViewCell.self)
        collectionView?.registerFooterView(withViewType: CollectionFooterLoadingView.self)
    }
    
    override func observeBindables() {
        self.bindDefaultObservables()
        observeReposList()
        observeSearchReposList()
        observeErrorMessage()
    }
    
    //MARK:- Helper functions
    
    func handleIfData(isEmpty: Bool = true) {
        self.collectionView.isHidden = isEmpty
        self.noDataFoundlbl.isHidden = !isEmpty
        
    }
    func observeReposList() {
        disposeBag.add(viewModel.reposList.bind { [weak self] repos in
            if repos.count > 0 {
                self?.handleIfData(isEmpty: false)
                DispatchQueue.main.async {
                    self?.collectionView.reloadData()
                }
            } else {
                self?.handleIfData(isEmpty: true)
            }
        })
    }
    
    func observeSearchReposList() {
        disposeBag.add(viewModel.searchReposList.bind { [weak self] repos in
            if repos.count > 0 {
                self?.handleIfData(isEmpty: false)
                DispatchQueue.main.async {
                    self?.collectionView.reloadData()
                }
            } else {
                self?.handleIfData(isEmpty: true)
            }
        })
    }
    
    func observeErrorMessage() {
        disposeBag.add(viewModel.errorMessage.bind { _ in
            self.noDataFoundlbl.text = "Something went wrong, please try again later"
        })
    }
    
    func setupSearchView()  {
        self.searchView.delegate = self
        self.searchView.placeholder = viewModel.searchPlaceholder
    }
    
    func showSearchView() {
        guard searchView?.alpha == 0 else { return }
        searchViewHeightConstraint?.constant = 70
        UIView.animate(withDuration: 0.25) {
            self.searchView?.alpha = 1
            self.view.layoutIfNeeded()
        }
    }
    
    func hideSearchView() {
        guard searchView?.alpha == 1 else { return }
        searchViewHeightConstraint?.constant = 0
        UIView.animate(withDuration: 0.25) {
            self.searchView?.alpha = 0
            self.view.layoutIfNeeded()
        }
    }

    func handleSearchBarStatus(for targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if targetContentOffset.pointee.y > 0.0 {
            hideSearchView()
        } else {
            showSearchView()
        }
    }
    
    func navigateToDetailsScreen(for index: Int) {
        let detailsVC = RepoDetailsViewController.instantiateFromStoryboard(.repoDetails)
        detailsVC.viewModel = RepoDetailsViewModel(repoModel: viewModel.getRepoModel(for: index))
        push(viewController: detailsVC)
    }
}

//MARK:- UICollectionView Delegate, data source, FlowLayout
extension ReposLisViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.listCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : RepoListCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.cellWidth = self.collectionView.frame.width
        if let cellViewModel = viewModel.getCellViewModel(for: indexPath.item) as? RepoListCellViewModel {
            cell.configure(with: cellViewModel)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        viewModel.getNextPageIfNeeded(indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigateToDetailsScreen(for: indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: 275)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footerView: CollectionFooterLoadingView = collectionView.dequeueReusableFooterView(forIndexPath: indexPath)
        footerView.animating = self.viewModel.shouldPaginate
        return footerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return kCollectionViewFooterSize
    }
}

//MARK:- UIScroll Delegates
extension ReposLisViewController {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        handleSearchBarStatus(for: targetContentOffset)
    }
}

extension ReposLisViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.searchFor(query: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}

