//
//  RepoDetailsViewModel.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/20/20.
//

import UIKit

class RepoDetailsViewModel: BaseViewModel {
    var loaderState = Bindable<LoadingIndicatorState>(.hidden)
    private var repoModel: Repo
    
    var repoName: String {
        return repoModel.full_name ?? "_"
    }
    
    var ownerName: String {
        return repoModel.owner?.login ?? "_"
    }
    
    var avatarImage: URL? {
        if UIApplication.shared.canOpenURL(URL(string: repoModel.owner?.avatar_url ?? "")!) {
            return URL(string: repoModel.owner?.avatar_url ?? "")
        }
        return nil
    }
    
    var repoDescription: String {
        return repoModel.description ?? "_"
    }
    
    init(repoModel: Repo) {
        self.repoModel = repoModel
    }
}
