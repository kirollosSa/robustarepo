//
//  RepoDetailsViewController.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/20/20.
//

import UIKit

class RepoDetailsViewController: BaseViewController, HasViewModel, StoryboardLoadable {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var repoImage: UIImageView!
    @IBOutlet weak var repoNamelbl: UILabel!
    @IBOutlet weak var ownerNamelbl: UILabel!
    @IBOutlet weak var repoDescriptionlbl: UILabel!
    
    //MARK:- View Model
    var viewModel: RepoDetailsViewModel!
    
    //MARK:- VC life cycle
    override func setupView() {
        containerView.backgroundColor = AppColors.lightGray
        containerView.setCornerRadius()
        repoNamelbl.font = UIFont.boldSystemFont(ofSize: 18)
        repoNamelbl.text = "Repo Name : " + viewModel.repoName
        ownerNamelbl.font = UIFont.systemFont(ofSize: 18)
        ownerNamelbl.text = "Owner Name : " +  viewModel.ownerName
        repoDescriptionlbl.numberOfLines = 0
        repoDescriptionlbl.lineBreakMode = .byWordWrapping
        repoDescriptionlbl.text = viewModel.repoDescription
        if let avatarImage = viewModel.avatarImage {
            repoImage.loadImage(from: avatarImage)
        } else {
            repoImage.image = UIImage(named: "dummy")
        }
    }
    
}
