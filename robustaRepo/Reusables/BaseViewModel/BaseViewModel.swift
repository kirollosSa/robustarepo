//
//  BaseViewModel.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/17/20.
//

import Foundation

protocol BaseViewModel : LoaderShowingManager {}

protocol HasViewModel: class {
    associatedtype ViewModel: BaseViewModel
    var viewModel: ViewModel! { get set }
    var disposeBag: DisposableBag { get set }
    func bindLoaderObservable()
}

/// HasViewModel for ViewControllers
extension HasViewModel {
    func bindDefaultObservables() {
        bindLoaderObservable()
    }
}

extension HasViewModel where Self: LoadingIndicatorShowing {
    func bindLoaderObservable() {
        disposeBag.add(
            viewModel.loaderState.bindAndFire({ [weak self] state in
                switch state {
                case .hidden:
                    DispatchQueue.main.async {
                        self?.loadingView.hideLoadingIndicator()
                    }
                case .shown:
                    DispatchQueue.main.async {
                        self?.loadingView.showLoadingIndicator()
                    }
                case .overlay:
                    DispatchQueue.main.async {
                        self?.loadingView.showOverlayLoadingIndicator()
                    }
                case .overlayWithColor(let color):
                    DispatchQueue.main.async {
                        self?.loadingView.showOverlayLoadingIndicator(with: color)
                    }
                }
            })
        )
    }
}


