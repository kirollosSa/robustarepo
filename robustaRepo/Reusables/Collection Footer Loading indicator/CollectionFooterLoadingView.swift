//
//  CollectionFooterLoadingView.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/18/20.
//

import UIKit

import UIKit

class CollectionFooterLoadingView: BaseCollectionReusableView {

    var loader: LoadingIndicator = LoadingIndicator(leading: 0, trailing: 0, top: 0, bottom: 0)
    var animating: Bool = false {
        didSet {
            if animating {
                loader.startAnimating()
            } else {
                loader.stopAnimating()
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    func setupView() {
        backgroundColor = AppColors.clear

        if animating {
            loader.startAnimating()
        }
        self.addSubview(loader)

        loader.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loader.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        loader.heightAnchor.constraint(equalToConstant: 30).isActive = true
        loader.widthAnchor.constraint(equalToConstant: 30).isActive = true
    }
}

