//
//  NavigationBarShowing.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/18/20.
//

import UIKit

protocol NavigationBarShowing: UIViewController {
    func hideNavigationBar(_ animated: Bool)
    func showNavigationBar(_ animated: Bool)
}


extension NavigationBarShowing {
    func hideNavigationBar(_ animated: Bool = true) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func showNavigationBar(_ animated: Bool = true) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}
