//
//  NavigationManaging.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/20/20.
//

import UIKit

protocol NavigationManaging {
    func push(viewController: UIViewController, animated: Bool)
}

extension NavigationManaging where Self: UIViewController {
    func push(viewController: UIViewController, animated: Bool = true) {
        navigationController?.pushViewController(viewController, animated: true)
    }
}

