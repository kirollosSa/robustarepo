//
//  ListingSearch.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/19/20.
//

import Foundation

protocol ListingSearch: class {
    var searchPlaceholder: String { get }
    func searchFor(query: String)
    var query: String { get set }
}

extension ListingSearch {
    var searchPlaceholder: String {
        return "Search"
    }
}
