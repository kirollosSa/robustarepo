//
//  BaseViewController.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/17/20.
//

import UIKit

@objc protocol BaseViewControllerCycle {
    func setupView()
    func registerNib()
    func observeBindables()
    func bindData()
}

class BaseViewController: UIViewController, BaseViewControllerCycle, NavigationBarShowing, NavigationManaging {

    var disposeBag = DisposableBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        registerNib()
        observeBindables()
        bindData()
    }
    
}

extension BaseViewController {
    func setupView() {}
    func registerNib() {}
    func observeBindables() {}
    func bindData() {}
}
