//
//  LoadingIndicatorShowing.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/16/20.
//

import Foundation
import UIKit

protocol LoadingIndicatorShowing {
    var loadingView: UIView! { get }
    func showLoadingIndicator()
    func showOverlayLoadingIndicator(with backgroundColor: UIColor)
    func hideLoadingIndicator()
}

class ViewTag {
    static let overlayView = 1001
    static let loaderView = 1002
    static let refreshControl = 1003
    static let loaderMessage = 1004
}

extension LoadingIndicatorShowing {
    var overlayView: UIView {
        if let view = loadingView.viewWithTag(ViewTag.overlayView) {
            return view
        }
        let view = UIView()
        view.tag = ViewTag.overlayView
        return view
    }
    var loader: LoadingIndicator {
        if let loader = loadingView.viewWithTag(ViewTag.loaderView) as? LoadingIndicator {
            return loader
        }
        let loader = LoadingIndicator()
        loader.tag = ViewTag.loaderView
        return loader
    }
    var messageLabel: UILabel {
        if let label = loadingView.viewWithTag(ViewTag.loaderMessage) as? UILabel {
            return label
        }
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = AppColors.black
        label.font = UIFont.systemFont(ofSize: 16)
        label.tag = ViewTag.loaderMessage
        return label
    }
}

// this allows us to show/hide loading indicators on any view
extension UIView: LoadingIndicatorShowing {
    var loadingView: UIView! {
        return self
    }
    
    // MARK: - Loading Indicator Handling
    func showLoadingIndicator() {
        DispatchQueue.main.async { [self] in
            guard !self.loadingView.subviews.contains(where: {$0 is LoadingIndicator }) else { return }
            loadingView.addSubview(self.loader)
            loader.translatesAutoresizingMaskIntoConstraints = false
            loader.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
            loader.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor).isActive = true
            loader.heightAnchor.constraint(equalToConstant: 70).isActive = true
            loader.widthAnchor.constraint(equalToConstant: 70).isActive = true
            loader.startAnimating()
        }
    }
    
    func showOverlayLoadingIndicator(with backgroundColor: UIColor = AppColors.white) {
        DispatchQueue.main.async { [self] in
            loadingView.addSubview(overlayView)
            overlayView.backgroundColor = backgroundColor
            overlayView.translatesAutoresizingMaskIntoConstraints = false
            overlayView.leadingAnchor.constraint(equalTo: loadingView.leadingAnchor).isActive = true
            overlayView.trailingAnchor.constraint(equalTo: loadingView.trailingAnchor).isActive = true
            overlayView.topAnchor.constraint(equalTo: loadingView.topAnchor).isActive = true
            overlayView.bottomAnchor.constraint(equalTo: loadingView.bottomAnchor).isActive = true
            overlayView.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
            overlayView.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor).isActive = true
            overlayView.showLoadingIndicator()
        }
    }
    
    func hideLoadingIndicator() {
        DispatchQueue.main.async { [self] in
            overlayView.removeFromSuperview()
            loader.stopAnimating()
            loader.removeFromSuperview()
            messageLabel.removeFromSuperview()
            messageLabel.text = nil
        }
    }
}

@objc extension UIViewController: LoadingIndicatorShowing {
    var loadingView: UIView! {
        return self.view
    }
    
    // MARK: - Loading Indicator Handling
    func showLoadingIndicator() {
        DispatchQueue.main.async {
            self.loadingView.showLoadingIndicator()
        }
    }
    
    func showOverlayLoadingIndicator(with backgroundColor: UIColor = AppColors.white) {
        DispatchQueue.main.async {
            self.loadingView.showOverlayLoadingIndicator(with: backgroundColor)
        }
    }
    
    func hideLoadingIndicator() {
        DispatchQueue.main.async {
            self.loadingView.hideLoadingIndicator()
        }
    }
}

enum LoadingIndicatorState: Equatable {
    case shown
    case overlay
    case overlayWithColor(color : UIColor)
    case hidden
}

protocol LoaderShowingManager {
    var loaderState: Bindable<LoadingIndicatorState> { get }
}
