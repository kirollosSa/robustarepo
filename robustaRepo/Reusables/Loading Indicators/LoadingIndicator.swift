//
//  LoadingIndicator.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/16/20.
//

import UIKit

class LoadingIndicator: UIView {

    var indicator: UIActivityIndicatorView!
    var containerView: UIView!
   
    init(frame: CGRect = CGRect(x: 0, y: 0, width: 30, height: 30), color: UIColor = AppColors.blue , leading :CGFloat = 10 ,trailing : CGFloat = 10 , top : CGFloat = 10 , bottom : CGFloat = 10 ) {
           super.init(frame: frame)
        self.indicator = UIActivityIndicatorView(frame: self.bounds)
        self.tintColor = color
        self.indicator.hidesWhenStopped = true
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addFitSubview(self.indicator, leading: leading, trailing: trailing , top: top, bottom: bottom)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func startAnimating() {
        self.indicator.startAnimating()
    }

    func stopAnimating() {
        self.indicator.stopAnimating()
    }
}

