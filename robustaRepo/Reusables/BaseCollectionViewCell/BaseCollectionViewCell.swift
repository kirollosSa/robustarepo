//
//  BaseCollectionViewCell.swift
//  robustaRepo
//
//  Created by Kirollos Maged on 12/18/20.
//

import UIKit

protocol UISetup {
    func setupView()
}

protocol Configurable {
    associatedtype ViewModel: BaseViewModel
    var viewModel: ViewModel! { get set }
    func configure(with viewModel: ViewModel)
}

extension UICollectionReusableView: NibLoadable, Reuseable {}

typealias BaseCollectionViewCell = UICollectionViewCell & UISetup & Configurable
typealias BaseCollectionReusableView = UICollectionReusableView & UISetup
